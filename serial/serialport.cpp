/***************************************************************************//**
 * Project Name : quicksms
 *
 * \file serialport.cpp this file contains some enums used by CSerialPort.
 * \package quicksms
 ******************************************************************************/
 
#include "serialport.h"

serial::ISerialPort::ISerialPort(const QString &pName,const serial::Baudrate &pBaudrate,const serial::Parity &pParity,
		  const serial::FlowControl &pFlow,const bool pOpen = 0,const ReadCallback pReadCallback = 0)
{
  if (!pName.isEmpty()) this->mName = pName;
  if (*mReadCallback > 0) this->mReadCallback = pReadCallback;
  
  this->mBaudrate = pBaudrate;
  this->mFlow = pFlow;
  
  if (pOpen) this->open();

};


serial::ISerialPort::~ISerialPort()
{
  if (this->isOpen()) this->close();
};


