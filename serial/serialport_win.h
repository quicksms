/***************************************************************************//**
 * Project Name : quicksms
 *
 * \file serialport_win.h this file contains CSerialPortWin class declaration.
 * \package quicksms
 ******************************************************************************/
 
#include "serialport.h"
#include "windows.h"

 
namespace serial
{
 
  class CSerialPortUNIX : public ISerialPort
  {
    public:
      
      //overloaded ISerialPort virtual method definitions begin below
      virtual bool open(const QString &pName);
      virtual bool open() = 0;
      virtual bool close( bool pForce = 0);
      virtual bool setOpened(const bool &pState);
      virtual bool isOpened(const bool &pState);
      virtual bool setBlocking(const bool &pState);
      virtual bool isBlocking(const bool &pState);
      virtual bool setBaudrate(const serial::Baudrate &pBaudrate);
      virtual bool setFlowControl(const serial::FlowControl &pFlow);
      virtual bool setParity(const serial::Parity &pParity);
      
      virtual int writeBuffer(void *pBuffer, unsigned int pSize);
      virtual int readBuffer(void *pBuffer, unsigned int pSize = 0, unsigned int pTimeout = 0);
      virtual int sendString(const QString &pStr);
      virtual int sendByteArray(const QByteArray &pArr);
      virtual int recieveString(QString &pStr, const unsigned int &pSize = 0);
      //overloaded ISerialPort virtual method defs end here.
      
    private:
	HANDLE mHandle; ///< holds file descriptor for an open port	
  }; //end class CSerialPortUnix;
  
};