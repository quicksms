/***************************************************************************//**
 * Project Name : quicksms
 *
 * \file serialdefs.h this file contains some enums used by CSerialPort.
 * \package quicksms
 ******************************************************************************/
#ifndef SERIALDEFS_H
#define SERIALDEFS_H

namespace serial
{

  /*********************************************************************//**
   * \brief parity.
   *
   * Use it to select correct port parity checking.
   ***********************************************************************/

  enum Parity
  {
    PAR_NONE, ///< no parity checking
    PAR_EVEN, ///< 'even' parity checking
    PAR_ODD ///< 'odd' parity checking
  };//Parity

  enum Baudrate
  {
    BAUD_DEFAULT
  };



 /*********************************************************************//**
  * \brief flow control.
  *
  * Used to select port flow control
  *
  ************************************************************************/
  enum FlowControl
  {
    FLOW_NONE, ///< no flow control
    FLOW_SOFTWARE, ///< XON/XOFF flow control
    FLOW_HARDWARE ///< hardware flow control
  };//FlowControl

  /*********************************************************************//**
   * \brief port status.
   * 
   * this enum is used to indicate port state. Constant names are
   * self-explainable.
   ************************************************************************/
  enum Status
  {
    STAT_OK, ///< everything's correct
    STAT_BUSY, ///< an r/w operation is in progress
    STAT_IO_ERROR, ///< I/O error occured 
    STAT_FILE_ALREADY_OPEN, ///< port is already open, maybe by another app, or you called open() twice?
    STAT_FILE_NOT_FOUND, ///< port name / path is incorrect.
    STAT_TIMEOUT, ///< a read / write / open timeout occured
    STAT_UNKNOWN ///< port uninitalized or in unknown state
  };//Status


 /*********************************************************************//**
  * \brief pointer to read callback function
  * \param[in] pSize how many bytes are available?
  * \param[in,out] pBuffer this points to recieved data
  *
  * if callback function is non-zero, then we can use it
  * to immediately process incoming data as soon as it becomes available
  *
  ************************************************************************/
  typedef int (*ReadCallback)(void* pBuffer, int &pSize);

};//serial

#endif //SERIALDEFS_H